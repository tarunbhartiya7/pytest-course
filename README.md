# Activate Virtual env

pipenv shell

# Run server

python manage.py runserver

# install any package

pipenv install pytest
pipenv -h - to get help commands of pipenv

# code formatting

pipenv install black --pre
run in terminal black .

# pytest

pytest -v -s -> get verbose output with any print statements

generate html report
pytest --html=demo-report.html --self-contained-html

setup pytest configuration
export DJANGO_SETTINGS_MODULE=todo.settings or configure in pytest.ini
